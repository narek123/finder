<?php

use yii\db\Migration;

class m160309_190801_create_roles extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('roles', [
            'id' => $this->primaryKey(),
            'role_name'=>$this->string(),
            'role_value'=>$this->integer(),
        ],$tableOptions);

        $this->insert('roles',[
            'role_name'=>'admin',
            'role_value'=> 10
        ]);
        $this->insert('roles',[
            'role_name'=>'superAdmin',
            'role_value'=> 20
        ]);
    }

    public function down()
    {
        $this->dropTable('roles');
    }
}
