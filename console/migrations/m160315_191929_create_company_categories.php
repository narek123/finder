<?php

use yii\db\Migration;

class m160315_191929_create_company_categories extends Migration
{
    public function up()
    {
        $this->createTable('company_categories', [
            'id' => $this->primaryKey(),
            'company_id'=>$this->integer(),
            'category_id'=>$this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_fk_cat_id','company_categories','category_id','categories','id','CASCADE','CASCADE');
    }

    public function down()
    {
        $this->dropTable('company_categories');
    }
}
