<?php

use yii\db\Migration;

class m160311_095223_insert_users extends Migration
{
    public function up()
    {
        $this->insert('user',[
            'username'=>'Narek',
            'email'=>'narek.amirkhanyan92@gmail.com',
            'password_hash'=>Yii::$app->security->generatePasswordHash(123456),
            'auth_key'=>Yii::$app->security->generateRandomString(),
            'gender_id'=>1,
            'role_id'=>30,
            'status'=>10,
            'created_at' =>date('Y-m-d H:i:s'),
            'updated_at' =>date('Y-m-d H:i:s'),
        ]);

        $this->insert('user',[
            'username'=>'Andranik',
            'email'=>'Andranik_Muradyan_1990@mail.ru',
            'password_hash'=>Yii::$app->security->generatePasswordHash(123456),
            'auth_key'=>Yii::$app->security->generateRandomString(),
            'gender_id'=>1,
            'role_id'=>30,
            'status'=>10,
            'created_at' =>date('Y-m-d H:i:s'),
            'updated_at' =>date('Y-m-d H:i:s'),
        ]);
    }

    public function down()
    {
        echo "m160311_095223_insert_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
