<?php

use yii\db\Migration;

class m160310_145644_create_statuses extends Migration
{
    public function up()
    {
        $this->createTable('statuses', [
            'id' => $this->primaryKey(),
            'status_name'=>$this->string(),
            'status_value'=>$this->integer(),
        ]);
        $this->insert('statuses',[
            'status_name'=>'active',
            'status_value'=>10,
        ]);

        $this->insert('statuses',[
            'status_name'=>'pending',
            'status_value'=>5,
        ]);
    }

    public function down()
    {
        $this->dropTable('statuses');
    }
}
