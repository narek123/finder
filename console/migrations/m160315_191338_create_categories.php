<?php

use yii\db\Migration;

class m160315_191338_create_categories extends Migration
{
    public function up()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'category_name'=>$this->string(),
            'parent_id'=>$this->integer()->defaultValue(0),
            'status'=>$this->integer()->defaultValue(1),
        ]);
    }

    public function down()
    {
        $this->dropTable('categories');
    }
}
