<?php

use yii\db\Migration;

class m160309_190829_create_genders extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('genders', [
            'id' => $this->primaryKey(),
            'gender_name'=>$this->string(),
        ],$tableOptions);

        $this->insert('genders',[
            'gender_name'=>'male'
        ]);

        $this->insert('genders',[
            'gender_name'=>'female'
        ]);
    }

    public function down()
    {
        $this->dropTable('genders');
    }
}
