<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\languageSwitcher;
use yii\bootstrap\Tabs;
/* @var $this yii\web\View */
/* @var $model backend\models\Message */

$this->title = $model->categoryTitle[$model->id].":".$model->messageTitle[$model->id];
$this->params['breadcrumbs'][] = ['label' => 'Messages', 'url' => ['/'.languageSwitcher::getLanguage().'/message/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['/'.languageSwitcher::getLanguage().'/message/update', 'id' => $model->id, 'language' => $model->language], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'language' => $model->language], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'language',
            'translation:ntext',
        ],
    ]) ?>

</div>
