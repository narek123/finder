<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\languageSwitcher;

/* @var $this yii\web\View */
/* @var $model backend\models\MessageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-search">

    <?php $form = ActiveForm::begin([
        'action' => ['/'.languageSwitcher::getLanguage().'/message/index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'language')->dropDownList($model->languageList,
        [
            'prompt'=>'Select language'
        ]) ?>

    <?= $form->field($model, 'translation')->dropDownList($model->translationList,
        [
            'prompt'=>'Select teanslation'
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
