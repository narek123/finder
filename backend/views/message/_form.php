<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\languageSwitcher;

/* @var $this yii\web\View */
/* @var $model backend\models\Message */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-form">

    <?php $form = ActiveForm::begin([
        'action'=>Yii::$app->request->baseUrl.'/'.languageSwitcher::getLanguage().'/message/update?id='
            .$model->id.'&language='.$model->language
    ]); ?>

    <?= $form->field($model, 'language')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'translation')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
