<?php

use yii\helpers\Html;
use yii\grid\GridView;
use anmaslov\autocomplete\AutoComplete;
use common\components\languageSwitcher;
use Zelenin\yii\modules\I18n\Module;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Messages';
$this->params['breadcrumbs'][] = $this->title;
echo Module::t('menu', 'about');
?>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <!--        --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'category',
                'value' => 'id0.category',
                'filter' => Html::activeDropDownList($searchModel, 'category', $searchModel->categoryList,
                    ['class' => 'form-control', 'prompt' => 'Select Category']),
            ],

            [
                'attribute' => 'message',
                'value' => 'id0.message',
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'message',
                    'data' => $searchModel->messageList,
                    'name' => 'MessageSearch[message]',
                    'options'=>[
                        'placeholder'=>'Message'
                    ],
                    'clientOptions' => [
                        'source' => $searchModel->messageList,
                    ]

                ]),
            ],

            [
                'attribute' => 'language',
                'value' => 'language',
                'filter' => Html::activeDropDownList($searchModel, 'language', $searchModel->languageList,
                    ['class' => 'form-control', 'prompt' => 'Select Language']),
            ],
            [
                'attribute' => 'translation',
                'value' => 'translation',
                'filter' => Html::activeDropDownList($searchModel, 'translation', $searchModel->translationList,
                    ['class' => 'form-control', 'prompt' => 'Select Translation']),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        $url = Yii::$app->request->baseUrl . '/' . languageSwitcher::getLanguage() . '/message/view?id='
                            . $model->id . '&language=' . $model->language;
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>',
                            $url);
                    },
                    'update' => function ($url, $model) {
                        $url = Yii::$app->request->baseUrl . '/' . languageSwitcher::getLanguage() . '/message/update?id='
                            . $model->id . '&language=' . $model->language;
                        return Html::a(
                            '<span class="glyphicon glyphicon-edit"></span>',
                            $url);

                    },
                    'delete' => function ($url, $model) {
                        $url = Yii::$app->request->baseUrl . '/' . languageSwitcher::getLanguage() . '/message/delete?id='
                            . $model->id . '&language=' . $model->language;
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'class' => 'classname',
                            'data' => [
                                'method' => 'POST',
                                'confirm' => 'Are you sure you want to delete this item?',
                                'params' => [
                                    'id' => $model->id,
                                    'language' => $model->language
                                ],
                            ]
                        ]);
                    },

                ],

            ],
        ],
    ]); ?>

</div>
