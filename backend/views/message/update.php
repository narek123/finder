<?php

use yii\helpers\Html;
use common\components\languageSwitcher;

/* @var $this yii\web\View */
/* @var $model backend\models\Message */

$this->title = 'Update Message: ' . ' ' . $model->messageTitle[$model->id];
$this->params['breadcrumbs'][] = ['label' => 'Messages', 'url' => ['/'.languageSwitcher::getLanguage().'/message/index']];
$this->params['breadcrumbs'][] = ['label' => $model->categorytitle[$model->id], 'url' => [
    '/'.languageSwitcher::getLanguage().'/message/view', 'id' => $model->id, 'language' => $model->language]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="message-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
