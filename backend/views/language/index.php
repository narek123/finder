<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\switchinput\SwitchInput;
use common\components\languageSwitcher;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\languageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Languages';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="language-index">

    <h1><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

//            'language_id',
        'language',
//            'country',
        'name',
        'name_ascii',
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return $model->status == 1 ? 'Active' : 'Blocked';
            },
            'filter' => Html::activeDropDownList($searchModel, 'status', array('blocked', 'active'),
                ['class' => 'form-control', 'prompt' => 'Select Status']),
        ],
        ['class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {change}',
            'buttons' => [
                'view' => function ($url, $model) {
                    $url = Yii::$app->request->baseUrl . '/' . languageSwitcher::getLanguage()
                        . '/language/view?id=' . $model->id;
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url, array('class' => 'btn btn-sm btn-warning'));
                },
                'update' => function ($url, $model) {
                    $url = Yii::$app->request->baseUrl . '/' . languageSwitcher::getLanguage()
                        . '/language/update?id=' . $model->id;
                    return Html::a(
                        '<span class="glyphicon glyphicon-pencil" ></span>',
                        $url, array('class' => 'btn btn-sm btn-info'));
                },
                'change' => function ($url, $model) {
                    $url = Yii::$app->request->absoluteUrl;
                    if ($model->status == 1) {
                        $img = 'minus';
                    } else {
                        $img = 'plus';
                    }
                    return "<span class='btn bth-sm btn-danger custom' data-key='$model->status'>
                                <span class='glyphicon glyphicon-$img'></span>
                            </span>";
                    }
                ],
            ],
        ],

    ]); ?>

</div>
