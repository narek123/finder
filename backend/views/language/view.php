<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\languageSwitcher;

/* @var $this yii\web\View */
/* @var $model backend\models\Language */

$this->title = $model->name.":".$model->name_ascii;
$this->params['breadcrumbs'][] = ['label' => 'Languages', 'url' => ['/'.languageSwitcher::getLanguage().'/language/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['/'.languageSwitcher::getLanguage().'/language/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'language_id',
            'language',
            'country',
            'name',
            'name_ascii',

            [
                'attribute' => 'status',
                'value' => $model->status === 1 ? 'Active' : 'Blocked',
            ],
        ],
    ]) ?>

</div>
