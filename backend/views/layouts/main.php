<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\components\languageSwitcher;
use kartik\sidenav\SideNav;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-top ',
        ],
    ]);
    if (Yii::$app->user->isGuest) {
        $navItems[] = ['label' => 'Login', 'url' => ['/' . languageSwitcher::getLanguage() . '/site/login']];
    } else {
        $navItems[] = '<li>'
            . Html::beginForm(['/' . languageSwitcher::getLanguage() . '/site/logout'], 'post')

            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $navItems,
    ]);
    ?>
    <?= languageSwitcher::Widget() ?>
    <?php
    NavBar::end();

    ?>
    <div class="container-fluid">
        <div class="col-sm-3">
            <?php
            if (!Yii::$app->user->isGuest) {
                $item='';
                echo SideNav::widget([
                    'type' => SideNav::TYPE_DEFAULT,
                    'items' => [
                        ['label' => 'Home',
                            'icon' => 'home',
                            'url' => Url::to(['/' . languageSwitcher::getLanguage() . '/site/index']),
                            'active' => ($item == 'home')
                        ],
                        ['label' => 'Translation',
                            'icon' => 'tag',
                            'url' => Url::to(['/' . languageSwitcher::getLanguage() . '/message/index']),
                            'active' => ($item == 'message')
                        ],
                        ['label' => 'Languages',
                            'icon' => 'tag',
                            'url' => Url::to(['/' . languageSwitcher::getLanguage() . '/language/index']),
                            'active' => ($item == 'language')
                        ],
                        ['label' => 'Add Admins',
                            'icon' => 'tag',
                            'url' => Url::to(['/' . languageSwitcher::getLanguage() . '/site/addadmins']),
                            'active' => ($item == 'addadmins')
                        ],
                        ['label' => 'Categories',
                            'icon' => 'tag',
                            'active' => ($item == 'categories'),
                            'items'=>[
                                ['label' => 'Categories',
                                    'icon' => 'tag',
                                    'url' => Url::to(['/' . languageSwitcher::getLanguage() . '/categories/index']),
                                    'active' => ($item == 'index')
                                ],
                                ['label' => 'Tree',
                                    'icon' => 'tag',
                                    'url' => Url::to(['/' . languageSwitcher::getLanguage() . '/categories/tree']),
                                    'active' => ($item == 'tree')
                                ],
                            ]
                        ],
                    ],
                ]);
            }
            ?>
        </div>
        <div class="col-sm-9">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
            <input class="baseUrl" type="hidden" value="<?= Yii::$app->request->baseUrl.'/'.languageSwitcher::getLanguage()?>">
        </div>
    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
