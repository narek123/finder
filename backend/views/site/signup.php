<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Admins */
/* @var $form ActiveForm */
?>
<div class="signup">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'password_hash')->passwordInput(); ?>
    <?= $form->field($model, 'confirm_password')->passwordInput();?>

    <?= $form->field($model, 'gender')->dropDownList($model->getGenderList(),
        [
            'prompt'=>'please choose one'
        ]) ;?>


    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
