<?php

namespace backend\controllers;

use common\components\languageSwitcher;
use Yii;
use backend\models\Categories;
use backend\models\CategoriesSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\helper;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Categories();
        if ((Yii::$app->request->isAjax)){
            $model->category_name=Yii::$app->request->post()['category_name'];
            $model->parent_id=Yii::$app->request->post()['id'];
            $model->save();
            var_dump(Yii::$app->request->post());die;
            
        }
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        if ((Yii::$app->request->isAjax)){
            $model = $this->findModel(Yii::$app->request->post()['id']);
            $model->category_name=Yii::$app->request->post()['category_name'];
            $res = $model->save();
            var_dump($res);die;

        }
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        if(Yii::$app->request->isAjax){
          $id=Yii::$app->request->post()['id'];
            $this->delete($id);
        }
        return $this->redirect(['/'.languageSwitcher::getLanguage().'/categories/tree']);
    }

    /**
     * @return string
     */
    public function actionTree(){
        return $this->render('tree');
    }

    /**
     * @return mixed
     */
    public function actionGet()
    {
        $model = new Categories();
        $category= $model->getCategoryList();
        return json_encode($category);
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     */
    protected function delete($id){
        $conn=Yii::$app->db;
        $comm=$conn->createCommand("SELECT * FROM categories WHERE parent_id = :id")
        ->bindValue(':id',$id);
         $res=$comm->queryAll();
        if(count($res)>0){
            foreach($res as $item){
                $this->delete($item['id']);
            }
        }
        $conn->createCommand('DELETE FROM categories WHERE id = :id')
        ->bindValue(':id',$id)->execute();

    }

}
