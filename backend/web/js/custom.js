function tree(obj, parent_id) {
    var st = 'display: inline-block;padding: 7px 10px;text-decoration: none;position: relative;min-width: 40px;z-index: 2;' +
        'background: #27a9e3;color: #fff;font-weight: bold;';
    var branch = '<ul style="list-style: none">';
    for (var item in obj) {
        if (obj[item]['parent_id'] == parent_id) {
            var children = tree(obj, obj[item]['id']);
            if (children) {
                branch = branch + '<li data-key="' + obj[item]['id'] + '" data-value="' + obj[item]['parent_id'] + '">' +
                    '<span class="vertical" style="height: 34px; margin-top: -34px; margin-left: 30px; left: 581.5px;"></span>' +
                    '<span class="horizontal" style="width: 103px; margin-top: -34px; margin-left: -73px; left: 581.5px;"></span>' +
                    '<div class="root" style="'+st+'">' +
                    '<span class=""></span>' +
                    '<b style="width: 11px;height: 12px;bottom: 0;left: -17px;cursor: pointer;">' +
                    '<span class="glyphicon glyphicon-minus-sign minus" style="display: inline"></span></b>' +
                    '<span class="glyphicon glyphicon-plus add-action" title="Click to add" style="display: none; cursor: pointer;"></span>' +
                    '<span class="glyphicon glyphicon-pencil edit-action" title="Click to edit" style="display: none; cursor: pointer;"></span>' +
                    '<span class="glyphicon glyphicon-remove-circle delete-action" title="Click to remove" style="display: none; cursor: pointer;"></span>' +
                    '<span class="glyphicon glyphicon-camera highlith" title="Click to highlith" style="display: none; cursor: pointer;"></span>'+
                    obj[item]["category_name"] + '</div></li>';
            }
            branch = branch + '<li>' + children + '</li>';
        }
    }
    return branch + '</ul>';
}
var tr;
$(document).ready(function () {
    var url = $('.baseUrl').val();
    var getUrl = window.location;
    var host = getUrl.protocol + "//" + getUrl.host;
    var id;
    var b = $('body');
    b.on('click', '.custom', function () {
        var s = $(this).attr('data-key');
        var id = $(this).parent().parent().attr('data-key');
        $.ajax({
            type: 'post',
            url: location.href,
            data: {status: s, id: id},
            success: function (result) {
                location.reload();
            }
        });
    });
    $.ajax({
        type: 'post',
        url: host + url + '/categories/get',
        success: function (res) {
            tr = JSON.parse(res);
            $('.tree').html(tree(tr, 0));
        }
    });
    b.on('click', '.save', function () {
        $.ajax({
            type: 'post',
            url: location.href,
            data: id + "=" + $("#inp").val(),
            success: function (res) {
                console.log(res)
            }
        });
    });
    b.on('click','.minus',function () {
        $(this).parent().parent().parent().next().css("display","none");
        $(this).removeClass('minus');
        $(this).removeClass('glyphicon-minus-sign');
        $(this).addClass('glyphicon-plus-sign');
        $(this).addClass('plus');
    });
    b.on('click','.plus',function () {
        $(this).parent().parent().parent().next().css("display","block");
        $(this).removeClass('plus');
        $(this).removeClass('glyphicon-plus-sign');
        $(this).addClass('glyphicon-minus-sign');
        $(this).addClass('minus');
    });
    b.on('mouseover','.root',function () {
        $(this).children('span').css('display','inline');
    });
    b.on('mouseout','.root',function () {
        $(this).children('span').css('display','none');
    });
    b.on('click','.add_root',function () {
        if(!$(this).parent().children('.form').length){
            $('<div class="form" style="color: black" id="add_root"><input class="inp" type="text" value=""><input class="call" type="button" value="submit"></div>').appendTo($(this).parent());
            b.find('span').css('pointer-events','none');
        }
    });
    b.on('click','.add-action',function () {
        if(!$(this).parent().children('.form').length){
            $('<div class="form" style="color: black" id="add_data"><input class="inp" type="text" value=""><input class="call" type="button" value="submit"></div>').appendTo($(this).parent());
            b.find('span').css('pointer-events','none');
        }
    });
    b.on('click','.edit-action',function () {
        if(!$(this).parent().children('.form').length){
            $('<div class="form" style="color: black" id="edite_data"><input class="inp" type="text" value=""><input class="call" type="button" value="submit"></div>').appendTo($(this).parent());
            b.find('span').css('pointer-events','none');
        }
    });
    b.on('click','.delete-action',function () {
        var id=$(this).parent().parent().attr('data-key');
        $(this).parent().parent().children('span').not('.add_root').css('display','none');
        if(confirm("You realy whant delete?")){
            $.ajax({
                type:'post',
                data:'id='+id,
                url:host+url+'/categories/delete',
                success:function () {
                    location.reload();
                }
            });
        }

    });
    b.on('click','.highlith',function () {
        var id=$(this).parent().parent().attr('data-key');
        $(this).parent().parent().children('span').not('.add_root').css('display','none');
        $('.tree').html(tree(tr, id));
        $("<span class='glyphicon glyphicon-hand-left back'></span>").appendTo(".tree");

    });
    b.on('click','.back',function () {
        $('.tree').html(tree(tr, 0))
    });
    b.on('click','.call',function () {
        var dest='';
        var ac=$(this).parent().attr("id"), v=$('.inp').val(), id=$(this).parent().parent().parent().attr('data-key');
        $(this).parent().parent().children('span').not('.add_root').css('display','none');
        switch (ac){
            case 'add_root': id=0;
                dest='/categories/create';
                break;
            case 'add_data': dest='/categories/create';
                break;
            case 'edite_data': dest='/categories/update';
                break;
        }
        $(this).parent().remove();
        b.find('span').css('pointer-events','auto');
       $.ajax({
           type:'post',
           data:'category_name='+v+'&id='+id,
           url:host+url+dest,
           success:function () {
               location.reload();
           }
       });
    })
});


