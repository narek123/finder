<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Message;
use Zelenin\yii\modules\I18n\models\SourceMessage;

/**
 * MessageSearch represents the model behind the search form about `backend\models\Message`.
 */
class MessageSearch extends Message
{
    public $language;
    public $translation;
    public $category;
    public $message;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['language', 'translation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();
        $query->joinWith('id0');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => array(
                'attributes' => [
                    'message',
                    'category',
                    'language',
                    'translation',
                ],
            ),
            'pagination' => array(
                'pageSize' => 8
            ),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(isset($params['MessageSearch']['translation']) && $params['MessageSearch']['translation']=== 'none'){
            $query->andWhere(
                'translation is null '
            );
        }else{
            $query->andFilterWhere([
                'translation'=>$this->translation,
            ]);
            $query->andFilterWhere(['like', 'translation', $this->translation]);
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'language' => $this->language,
            'source_message.category' => $this->category,

        ]);

        $query->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'source_message.message', $this->message]);

        return $dataProvider;
    }
}
