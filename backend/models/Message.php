<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\ValueHelpers;
/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property SourceMessage $id0
 */
class Message extends SourceMessage
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'translation' => 'Translation',
            'category' => 'Category',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }


    /**
     * get all messages
     * @return array
     */
    public function getMessageList()
    {
        $arg=$this->find()->from('source_message')
            ->innerJoin('message','source_message.id=message.id')
            ->asArray()->all();
        return ValueHelpers::changeValuess(ArrayHelper::map($arg,'id','message'));
    }

    /**
     * get language list
     * @return array
     */
    public function getLanguageList()
    {
        $dropoptions=Message::find()->asArray()->all();
        $arr=ArrayHelper::map($dropoptions,'language','id');
        return ValueHelpers::changeKeys($arr);
    }

    /**
     * get language list
     * @return array
     */
    public function getTranslationList()
    {
        $dropoptions=Message::find()->asArray()->all();
        $arr=ArrayHelper::map($dropoptions,'translation','id');
        return ValueHelpers::changeKeys($arr);
    }

    /**
     * get categories
     * @return array
     */
    public function getCategoryList()
    {
        $arg=$this->find()->from('source_message')
            ->innerJoin('message','source_message.id=message.id')
            ->asArray()->all();
        return ValueHelpers::changeValuess(ArrayHelper::map($arg,'id','category'));
    }

    public function getCategoryTitle()
    {
        $arg=$this->find()->from('source_message')
            ->innerJoin('message','source_message.id=message.id')
            ->asArray()->all();
        return ArrayHelper::map($arg,'id','category');
    }
    /**
     * get categories
     * @return array
     */
    public function getMessageTitle()
    {
        $arg=$this->find()->from('source_message')
            ->innerJoin('message','source_message.id=message.id')
            ->asArray()->all();
        return ArrayHelper::map($arg,'id','message');
    }
}
