<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $language_id
 * @property string $language
 * @property string $country
 * @property string $name
 * @property string $name_ascii
 * @property integer $status
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'language', 'country', 'name', 'name_ascii', 'status'], 'required'],
            [['status'], 'integer'],
            [['language_id'], 'string', 'max' => 5],
            [['language', 'country'], 'string', 'max' => 3],
            [['name', 'name_ascii'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language_id' => 'Language ID',
            'language' => 'Language',
            'country' => 'Country',
            'name' => 'Name',
            'name_ascii' => 'Name Ascii',
            'status' => 'Status',
        ];
    }

    /**
     * get language code and name
     * @return array
     */
    public static function getLanguage()
    {
        $language=Language::find()->where('status = 1')->asArray()->all();
        return ArrayHelper::map($language,'language','name');
    }

    /**
     * get language code and language id
     * @return array
     */
    public static function getLanguageId()
    {
        $language=Language::find()->where('status = 1')->asArray()->all();
        return ArrayHelper::map($language,'id','language');
    }

    /**
     * get status list
     * @return array
     */
    public function getStatusList()
    {
        $language=Language::find()->asArray()->all();
        return ArrayHelper::map($language,'id','satus');
    }
}
