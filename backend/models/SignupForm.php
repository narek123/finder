<?php
namespace backend\models;


use common\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password_hash;
    public $confirm_password;
    public $gender;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password_hash', 'required'],
            ['password_hash', 'string', 'min' => 6],
            ['confirm_password','compare','compareAttribute'=>'password_hash', 'message'=>'passwords dont equal'],
            ['confirm_password','required'],


            ['gender', 'required'],
            ['gender', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'email' => 'Email',
            'password_hash' => 'Password',
            'confirm-password'=>'Confirm Password',
            'gender' => 'Gender',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new Admins();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setGender($this->gender);
        $user->setPassword($this->password_hash);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }

    public function getGender()
    {
        $gender=new ActiveRecord();
        return $gender->hasOne(Genders::className(), ['id' => 'gender_id']);
    }

    public function getGenderList()
    {
        $dropoptions=Genders::find()->asArray()->all();
        return ArrayHelper::map($dropoptions,'id','gender_name');
    }
}
