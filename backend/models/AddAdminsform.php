<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use Zelenin\yii\modules\I18n\Module;




class AddAdminsform extends User
{
    public $username;
    public $email;
    public $password_hash;
    public $gender;
    public $confirm_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password_hash', 'required'],
            ['password_hash', 'string', 'min' => 6],
            ['confirm_password', 'compare', 'compareAttribute' => 'password_hash', 'message' => 'passwords dont equal'],
            ['confirm_password', 'required'],


            ['gender', 'required'],
            ['gender', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Module::t('login','username'),
            'email' => 'Email',
            'password_hash' => 'Password',
            'confirm-password' => 'Confirm Password',
            'gender' => 'Gender',
        ];
    }


    public function admins()
{
    if (!$this->validate()) {
        return null;

    }

    $user = new User();
    $user->username = $this->username;
    $user->email = $this->email;
    $user->setGender((int) $this->gender);
    $user->setPassword($this->password_hash);
    $user->generateAuthKey();

    return $user->save() ? $user : null;
}

    public function getGender()
    {

        $gender=new ActiveRecord();
        return $gender->hasOne(Genders::className(), ['id' => 'gender_id']);
    }

    public function getGenderList()
    {
        $dropoptions=Genders::find()->asArray()->all();
        return ArrayHelper::map($dropoptions,'id','gender_name');
    }

public function getRadioList(){
    $dropoptions=Genders::find()->asArray()->all();
    $return= ArrayHelper::map($dropoptions,'id','gender_name');
    return Html::activeRadio($return,'id','gender_name');

   }

}


?>