<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'i18n' => \Zelenin\yii\modules\I18n\Module::className()
    ],
    'components' => [
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => array(
                '<lang:\w+>/<controller>/<action>/<id:\d+>/<title>' => '<controller>/<action>',
                '<lang:\w+>/<controller>/<id:\d+>/<title>' => '<controller>/index',
                '<lang:\w+>/<controller>/<action>/<id:\d+>' => '<controller>/<action>',
                '<lang:\w+>/<controller>/<action>' => '<controller>/<action>',
                '<lang:\w+>/<controller>' => '<controller>',
                '<lang:\w+>/' => 'site/index',

            ),
        ],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
