<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'zelenin/yii2-semantic-ui' => 
  array (
    'name' => 'zelenin/yii2-semantic-ui',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@Zelenin/yii/SemanticUI' => $vendorDir . '/zelenin/yii2-semantic-ui',
    ),
  ),
  'zelenin/yii2-i18n-module' => 
  array (
    'name' => 'zelenin/yii2-i18n-module',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@Zelenin/yii/modules/I18n' => $vendorDir . '/zelenin/yii2-i18n-module',
    ),
    'bootstrap' => 'Zelenin\\yii\\modules\\I18n\\Bootstrap',
  ),
  'anmaslov/yii2-autocomplete-widget' => 
  array (
    'name' => 'anmaslov/yii2-autocomplete-widget',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@anmaslov/autocomplete' => $vendorDir . '/anmaslov/yii2-autocomplete-widget/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.2.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-switchinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-switchinput',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@kartik/switchinput' => $vendorDir . '/kartik-v/yii2-widget-switchinput',
    ),
  ),
  'kartik-v/yii2-widget-sidenav' => 
  array (
    'name' => 'kartik-v/yii2-widget-sidenav',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/sidenav' => $vendorDir . '/kartik-v/yii2-widget-sidenav',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'creocoder/yii2-nested-sets' => 
  array (
    'name' => 'creocoder/yii2-nested-sets',
    'version' => '0.9.0.0',
    'alias' => 
    array (
      '@creocoder/nestedsets' => $vendorDir . '/creocoder/yii2-nested-sets/src',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.4.7.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform',
    ),
  ),
  'kartik-v/yii2-tree-manager' => 
  array (
    'name' => 'kartik-v/yii2-tree-manager',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/tree' => $vendorDir . '/kartik-v/yii2-tree-manager',
    ),
  ),
);
