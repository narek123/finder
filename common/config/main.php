<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'languageSwitcher',
    ],
    'components' => [
        'i18n' => [
            'class'=> \Zelenin\yii\modules\I18n\components\I18N::className(),
            'languages' => ['en_US', 'ru_RU', 'am_AM'],
            'translations' => [
                'yii' => [
                    'class' => \yii\i18n\DbMessageSource::className()
                ]
            ]
        ],
        'languageSwitcher' => [
            'class' => 'common\components\languageSwitcher',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
