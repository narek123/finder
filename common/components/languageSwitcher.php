<?php

namespace common\components;

use Yii;
use yii\base\Widget;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use backend\models\Language;
use yii\web\Cookie;

class languageSwitcher extends Widget
{
    /**
     * @var string
     */
    public $default_lang="en";
    /**
     * @var
     */
    public $language;
    /**
     *
     * @var array
     */
    public static $languages = [];

    /**
     * @return bool
     */
    public function init()
    {
        self::$languages = Language::getLanguage();
        Yii::$app->getI18n()->languages=self::Languages();
        if (php_sapi_name() === 'cli') {
            return true;
        }

        parent::init();

        $cookies = Yii::$app->response->cookies;
        $languageNew = Yii::$app->request->get('lang');
        if ($languageNew) {
            if (isset(self::$languages[$languageNew])) {
                Yii::$app->language = $languageNew;
                $cookies->add(new Cookie([
                    'name' => 'language',
                    'value' => $languageNew
                ]));
            }
        } elseif ($cookies->has('language')) {

            Yii::$app->language = $cookies->getValue('lang');
        }
        else{

            $languageNew=$this->default_lang;
            Yii::$app->language = $languageNew;
            $cookies->add(new Cookie([
                'name' => 'language',
                'value' => $languageNew
            ]));
        }

    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $languages = self::$languages;
        $current = $languages[Yii::$app->language];
        unset($languages[Yii::$app->language]);

        $items = [];
        foreach ($languages as $code => $language) {
            $temp = [];
            $temp['label'] = $language;
            $temp['url'] = Url::current(['lang' => $code]);
            array_push($items, $temp);
        }
        echo ButtonDropdown::widget([
            'options'=>[
                'class'=>'navbar-right',
            ],
            'label' => $current,
            'dropdown' => [
                'items' => $items,
            ],
        ]);
    }

    /**
     * @return array|mixed
     */
    public static function getLanguage()
    {
        if(Yii::$app->request->get('lang')){
            $languages=array_flip(self::$languages);
            $newLanguage=Yii::$app->request->get('lang');
            if(in_array($newLanguage,$languages)){
                Yii::$app->language=$newLanguage;
               return Yii::$app->request->get('lang');
            }else{
                Yii::$app->language = Yii::$app->response->cookies->getValue('lang');
                return Yii::$app->response->cookies->getValue('language');
            }
        }
        else{
            Yii::$app->language = Yii::$app->response->cookies->getValue('language');
            return Yii::$app->response->cookies->getValue('language');
        }
    }

    public static function Languages(){
        $newArray=[];
        foreach(Language::getLanguageId() as $value){
            $newArray[]=$value;
        }
        return $newArray;
    }
}