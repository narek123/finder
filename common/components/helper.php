<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 19.03.2016
 * Time: 13:08
 */

namespace common\components;

use yii\helpers\Json;
class helper
{

    public static function tree(array $elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = self::tree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    public static function getCildId(array  $elements, $parentId)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = self::tree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element['id'];
            }
        }
        return $branch;
    }
    
}