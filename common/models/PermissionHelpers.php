<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 17.02.2016
 * Time: 17:02
 */

namespace common\models;

use yii;

class PermissionHelpers
{
    /**
     * @requireStatus
     * used two lines for if statement to avoid word wrapping
     * @param mixed $status_name
     * @return boolean
     */
    public static function requireStatus($status_name)
    {
        if ( Yii::$app->user->identity->status ==
            ValueHelpers::getStatusValue($status_name)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @requireMinimumStatus
     * used two lines for if statement to avoid word wrapping
     * @param mixed $status_name
     * @return boolean
     */
    public static function requireMinimumStatus($status_name)
    {
        if ( Yii::$app->user->identitstatus >=
            ValueHelpers::getStatusValue($status_name)) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * @requireRole
     * used two lines for if statement to avoid word wrapping
     * @param mixed $role_name
     * @return  boolean
     */
    public static function requireRole($role_name)
    {
        if ( Yii::$app->user->identity->role_id ==
            ValueHelpers::getRoleValue($role_name)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @requireMinimumRole
     * used two lines for if statement to avoid word wrapping
     * @param mixed $role_name
     * @return boolean
     */
    public static function requireMinimumRole($role_name)
    {
        if ( Yii::$app->user->identity->role_id >=
            ValueHelpers::getRoleValue($role_name)) {

            return true;
        } else {
            return false;
        }
    }
}