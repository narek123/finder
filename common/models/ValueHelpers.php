<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 17.02.2016
 * Time: 16:39
 */

namespace common\models;

use Yii;
class ValueHelpers
{
    /**
     * return the value of a role name handed in as string
     * example: 'Admin'
     *
     * @param mixed $role_name
     */
    public static function getRoleValue($role_name)
    {
        $connection = Yii::$app->db;
        $sql = "SELECT role_value FROM roles WHERE role_name=:role_name";
        $command = $connection->createCommand($sql);
        $command->bindValue(":role_name", $role_name);
        $result = $command->queryOne();
        return $result['role_value'];
    }
    /**
     * return the value of a status name handed in as string
     * example: 'Active'
     * @param mixed $status_name
     */
    public static function getStatusValue($status_name)
    {
        $connection = Yii::$app->db;
        $sql = "SELECT status_value FROM statuses WHERE status_name=:status_name";
        $command = $connection->createCommand($sql);
        $command->bindValue(":status_name", $status_name);
        $result = $command->queryOne();
        return $result['status_value'];
    }

    public static function changeKeys($array)
    {
        $newArr=[];
        foreach($array as $key => $value){
            if($key === ''){
                $newArr['none']='Not set';
            }else{
                $newArr[$key]=$key;
            }
        }
        return $newArr;
    }

    public static function changeValuess($array)
    {
        $newArr=[];
        foreach($array as $key => $value){
            $newArr[$value]=$value;
        }
        return $newArr;
    }

}