<?php
namespace common\models;

use Yii;
use yii\base\Model;
use Zelenin\yii\modules\I18n\Module;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password_hash;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email','password_hash'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            //email validate
            ['email','email'],
            // password is validated by validatePassword()

            ['password_hash', 'validatePassword'],
            ['password_hash', 'string', 'min'=>6],




        ];
    }
    public function attributeLabels()
    {
        return [
            'email' => Module::t('login','email'),
            'password_hash' =>  Module::t('login','password'),
        ];
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password_hash)) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate() && $this->getUser()->status ==
            ValueHelpers::getStatusValue('active')) {
            return Yii::$app->user->login($this->getUser(),
                $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}
